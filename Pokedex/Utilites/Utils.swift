//
//  Utils.swift
//  Pokedex
//
//  Created by Mazen Denden on 13/7/2022.
//

import SwiftUI

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

extension Int {
    func stringWithZeros() -> String {
        if self < 10 { return "00\(self)" }
        if self < 100 { return "0\(self)" }
        return "\(self)"
    }
}
