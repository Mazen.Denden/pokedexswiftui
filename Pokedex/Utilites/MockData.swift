//
//  MockData.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

struct MockData {
    
    static func singlePokemon() -> Pokemon {
        return Pokemon(id: 1,
                       abilities: [
                        PokemonAbility(ability: Ability(name: "Overgrow")),
                        PokemonAbility(ability: Ability(name: "Chlorophyl"))
                       ],
                       name: "bulbasaur",
                       species: Specie(url: "https://pokeapi.co/api/v2/pokemon-species/1/",
                                       genera: [
                                        Genus(genus: "Pokémon Graine", language: Language(name: "fr")),
                                        Genus(genus: "Seed Pokémon", language: Language(name: "en"))
                                       ],
                                       eggGroups: [
                                        EggGroup(name: "monster"),
                                        EggGroup(name: "plant")
                                       ],
                                       habitat: Habitat(name: "grassland")),
                       sprites: PokemonSprite(other: PokemonArtwork(officialArtwork: OfficialArtwork(frontDefault: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png"))),
                       stats: [
                        PokemonStat(baseStat: 45, stat: Stat(name: "hp")),
                        PokemonStat(baseStat: 49, stat: Stat(name: "attack")),
                        PokemonStat(baseStat: 49, stat: Stat(name: "defense")),
                        PokemonStat(baseStat: 65, stat: Stat(name: "special-attack")),
                        PokemonStat(baseStat: 65, stat: Stat(name: "special-defense")),
                        PokemonStat(baseStat: 45, stat: Stat(name: "speed"))
                       ],
                       types: [
                        PokemonTypeSlot(slot: 1, type: PokemonType(name: "grass", url: "https://pokeapi.co/api/v2/type/12/")),
                        PokemonTypeSlot(slot: 2, type: PokemonType(name: "poison", url: "https://pokeapi.co/api/v2/type/4/"))
                       ],
                       height: 7,
                       weight: 69
        )
    }
    
    static func singleMock() -> PokemonInfo {
        return PokemonInfo(name: "bulbasaur", url: "https://pokeapi.co/api/v2/pokemon/1/")
    }
    
    static func mock() -> [PokemonInfo] {
        return [
            PokemonInfo(name: "bulbasaur", url: "https://pokeapi.co/api/v2/pokemon/1/"),
            PokemonInfo(name: "charmander", url: "https://pokeapi.co/api/v2/pokemon/4/")
            //            PokemonInfo(name: "squirtle", url: "https://pokeapi.co/api/v2/pokemon/7/")
        ]
    }
}
