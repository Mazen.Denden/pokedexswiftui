//
//  ApiError.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//

enum ApiError: Error {
    case invalidURL
    case unableToComplete
    case invalidResponse
    case invalidData
    case failedToDecode
}

extension ApiError: CustomStringConvertible {
    var description: String {
        switch self {
        case .invalidURL: return "Invalid URL"
        case .unableToComplete: return "Unable to compelte request"
        case .invalidResponse: return "Invalid response"
        case .invalidData: return "Invalid Data"
        case .failedToDecode: return "Could not correctly decode JSON"
        }
    }
}
