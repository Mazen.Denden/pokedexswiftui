//
//  NetworkManager.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import Foundation

final class NetworkManager {
    static let shared = NetworkManager()
    let decoder = JSONDecoder()
    let urlSession = URLSession.shared
}
