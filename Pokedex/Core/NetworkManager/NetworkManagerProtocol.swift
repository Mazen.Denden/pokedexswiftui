//
//  NetworkManagerProtocol.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 21/6/2022.
//
import Foundation

protocol NetworkManagerProtocol {
    func fetch<T: Decodable>(type: T.Type, from endPoint: Endpoint, completion: @escaping (Result<T, ApiError>) -> Void)
    func fetch<T: Decodable>(type: T.Type, from url: URL?, completion: @escaping (Result<T, ApiError>) -> Void)
}
