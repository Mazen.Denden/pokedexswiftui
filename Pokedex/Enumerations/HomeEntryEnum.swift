//
//  HomeEntryEnum.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

import SwiftUI

enum HomeEntry: String, CaseIterable {
    case pokedex
    case moves
    case abilities
    case items
    case locations
    case typeCharts
    
    var title: String {
        switch self {
        case .pokedex: return "Pokedex"
        case .moves: return "Moves"
        case .abilities: return "Abilities"
        case .items: return "Items"
        case .locations: return "Locations"
        case .typeCharts: return "Type Charts"
        }
    }
    
    var backgroundColor: Color {
        switch self {
        case .pokedex: return TypeColor.grass.color
        case .moves: return TypeColor.fire.color
        case .abilities: return TypeColor.water.color
        case .items: return TypeColor.electric.color
        case .locations: return TypeColor.ghost.color
        case .typeCharts: return TypeColor.rock.color
        }
    }
}
