//
//  PokemonTypeColorEnum.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

import SwiftUI

enum TypeColor: String {
    case bug
    
    case dark
    case dragon
    
    case electric
    
    case fairy
    case fighting
    case fire
    case flying
    
    case ghost
    case grass
    case ground
    
    case ice
    
    case normal
    
    case poison
    case psychic
    
    case rock
    
    case shadow
    case steel
    
    case unknown
    
    case water
    
    var color: Color {
        switch self {
        case .bug: return Color(red: 168/255, green: 184/255, blue: 31/255)
            
        case .dark: return Color(red: 112/255, green: 88/255, blue: 72/255)
        case .dragon: return Color(red: 112/255, green: 56/255, blue: 248/255)
            
        case .electric: return Color(red: 248/255, green: 208/255, blue: 48/255)
            
        case .fairy: return Color(red: 248/255, green: 160/255, blue: 224/255)
        case .fighting: return Color(red: 144/255, green: 48/255, blue: 40/255)
        case .fire: return Color(red: 240/255, green: 80/255, blue: 48/255)
        case .flying: return Color(red: 168/255, green: 144/255, blue: 240/255)
            
        case .ghost: return Color(red: 112/255, green: 88/255, blue: 152/255)
        case .grass: return Color(red: 120/255, green: 200/255, blue: 80/255)
        case .ground: return Color(red: 224/255, green: 192/255, blue: 104/255)
            
        case .ice: return Color(red: 152/255, green: 216/255, blue: 216/255)
            
        case .normal: return Color(red: 168/255, green: 168/255, blue: 121/255)
            
        case .poison: return Color(red: 160/255, green: 64/255, blue: 160/255)
        case .psychic: return Color(red: 248/255, green: 88/255, blue: 136/255)
            
        case .rock: return Color(red: 184/255, green: 160/255, blue: 56/255)
            
        case .shadow: return Color(red: 64/255, green: 50/255, blue: 70/255)
        case .steel: return Color(red: 184/255, green: 184/255, blue: 208/255)
            
        case .unknown: return Color(red: 104/255, green: 160/255, blue: 144/255)
            
        case .water: return Color(red: 104/255, green: 144/255, blue: 240/255)
        }
    }
}
