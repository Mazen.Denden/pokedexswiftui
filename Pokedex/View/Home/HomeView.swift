//
//  HomeView.swift
//  Pokedex
//
//  Created by Mazen Denden on 2/6/2022.
//

import SwiftUI

struct HomeView: View {
    
    let columns: [GridItem] = Array(repeating: .init(.flexible(), spacing: 16), count: 2)
    let items: [HomeEntry] = HomeEntry.allCases
    
    var body: some View {
        NavigationView {
            VStack {
                VStack(alignment: .leading) {
                    Text("What Pokemon are you looking for?")
                        .font(.system(size: 34, weight: .heavy))
                        .padding(.vertical, 42)
                    
                    LazyVGrid(columns: columns, spacing: 16) {
                        ForEach(items, id: \.self) { homeEntry in
                            NavigationLink {
                                switch homeEntry {
                                case .pokedex: PokedexView()
                                default: Text("Work In Progress")
                                }
                            } label: {
                                HomeCell(homeEntry: homeEntry)
                            }
                        }
                    }
                }
                .padding()
                .background(Color.white)
                .cornerRadius(32, corners: [.bottomLeft, .bottomRight])
                .ignoresSafeArea()
                
                Spacer()
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
            
            .background(Color.gray.opacity(0.1))
        }
        .accentColor(.black)
    }
    
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct HomeCell: View {
    
    let homeEntry: HomeEntry
    
    var body: some View {
        HStack {
            Text(homeEntry.title)
                .font(.system(size: 14))
                .fontWeight(.semibold)
                .foregroundColor(.white)
                .padding(.leading, 16)
            
            Spacer()
            
            Color.clear
                .aspectRatio(1, contentMode: .fit)
                .overlay(
                    Image("pokeball-empty")
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 60)
                        .tint(Color.white.opacity(0.5))
                    )
                .clipShape(Circle())
                .rotationEffect(Angle(degrees: 25))
        }
        .frame(height: 60)
        .background {
            RoundedRectangle(cornerRadius: 15, style: .continuous)
                .fill(homeEntry.backgroundColor)
        }
    }
}
