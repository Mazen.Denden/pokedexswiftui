//
//  PokemonViewModel.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

import SwiftUI
import Combine

class PokemonViewModel: ObservableObject, Identifiable {
    private var cancellables = Set<AnyCancellable>()
    
    @Published var pokemon: Pokemon?
    @Published var error: ApiError?
    
    var pokemonUrl: String = ""
    let pokemonService: PokemonServiceProtocol
    
    init(pokemonService: PokemonServiceProtocol = PokemonService(), pokemonUrl: String = "") {
        self.pokemonUrl = pokemonUrl
        self.pokemonService = pokemonService
    }
    
    init(pokemonService: PokemonServiceProtocol = PokemonService(), pokemon: Pokemon) {
        self.pokemon = pokemon
        self.pokemonService = pokemonService
    }
    
    let id: UUID = UUID()
    
    var number: Int {
        return pokemon?.id ?? 0
    }
    
    var numberString: String {
        return "#" + number.stringWithZeros()
    }
    
    var name: String {
        return pokemon?.name.capitalized ?? ""
    }
    
    var types: [String] {
        return pokemon?.types.map { $0.type?.name.capitalized ?? "" } ?? []
    }
    
    var pokemonImageURL: URL? {
        return URL(string: pokemon?.sprites?.other?.officialArtwork?.frontDefault ?? "")
    }
    
    var pokemonTypeColor: Color {
        return pokemon?.types.first?.type?.color ?? Color.gray.opacity(0.2)
    }
    
    var specieURL: String {
        return pokemon?.species?.url ?? ""
    }

    var specieName: String {
        return pokemon?.species?.genera?.filter { $0.language?.name == "en" }.first?.genus ?? ""
    }
    
    var height: String {
        let height = pokemon?.height ?? 0
        let heightInMeters = Float(Float(height) / 10)
        return "\(heightInMeters) m"
    }
    
    var weight: String {
        let weight = pokemon?.weight ?? 0
        let weightInKg = Float(Float(weight) / 10)
        return "\(weightInKg) kg"
    }
    
    var abilities: String {
        return pokemon?.abilities.map { $0.ability?.name.capitalized ?? "" }.joined(separator: ", ") ?? ""
    }
    
    var eggGroups: String {
        return pokemon?.species?.eggGroups?.map { $0.name.capitalized }.joined(separator: ", ") ?? ""
    }
    
    var habitat: String {
        return pokemon?.species?.habitat?.name.capitalized ?? ""
    }
    
    var stats: [PokemonStat] {
        return pokemon?.stats ?? []
    }
    
    var flavorText: String {
        let notReturnToLignText = pokemon?.species?.flavorTextEntries?.first(where: { $0.language?.name == "en" })?.flavorText.replacingOccurrences(of: "\n", with: " ") ?? ""
        return notReturnToLignText
    }
    
}

extension PokemonViewModel {
    func fetchPokemon() {
        pokemonService.fetchPokemon(pokemonUrl: pokemonUrl) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let pokemon):
                    self?.pokemon = pokemon
                case .failure(let error):
                    self?.error = error
                }
            }
        }
    }
    
    func fetchPokemonSpecie() {
        pokemonService.fetchPokemonSpecie(specieURL: specieURL) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let specie):
                    self?.pokemon?.species = specie
                case .failure(let error):
                    self?.error = error
                }
            }
        }
        
    }
}
