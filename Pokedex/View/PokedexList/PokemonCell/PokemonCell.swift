//
//  PokemonCell.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

import SwiftUI
import Kingfisher

struct PokemonCell: View {
    
    @StateObject var viewModel = PokemonViewModel()
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                
                Text(viewModel.numberString)
                    .font(.system(size: 12, weight: .semibold))
                    .foregroundColor(.white.opacity(0.5))
            }
            
            Text(viewModel.name)
                .font(.system(size: 12, weight: .semibold))
                .foregroundColor(.white)
            
            HStack {
                VStack(alignment: .leading) {
                    ForEach(viewModel.types, id: \.self) { type in
                        Text(type)
                            .font(.system(size: 11, weight: .regular))
                            .foregroundColor(.white)
                            .padding(4)
                            .background {
                                RoundedRectangle(cornerRadius: 5, style: .continuous)
                                    .fill(.white.opacity(0.2))
                            }
                    }
                    
                    Spacer()
                }
                
                Spacer()
                
                if let url = viewModel.pokemonImageURL {
                    KingFisherImage(url: url, aspect: .fill)
                        .frame(width: 70)
                }
            }
            
        }
        .padding()
        .frame(height: 120)
        .background {
            RoundedRectangle(cornerRadius: 16, style: .continuous)
                .fill(viewModel.pokemonTypeColor)
        }
        .shadow(color: viewModel.pokemonTypeColor, radius: 3, x: 0, y: 0)
        .onAppear {
            viewModel.fetchPokemon()
        }
        
    }
}

struct PokemonCell_Previews: PreviewProvider {
    static var previews: some View {
        PokedexView()
    }
}
