//
//  PokedexViewModel.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

import SwiftUI

final class PokedexViewModel: ObservableObject {
    
    let columns: [GridItem] = Array(repeating: .init(.flexible(), spacing: 12), count: 2)
    
    @Published var pokemon: [PokemonViewModel] = []
    @Published var error: ApiError?
    
    let service: PokedexListServiceProtocol
    init(service: PokedexListServiceProtocol = PokedexListService()) {
        self.service = service
    }
    
}

extension PokedexViewModel {
    func fetchPokemon(limit: Int, offset: Int) {
        service.fetchPokemon(limit: limit, offset: offset) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let pokemonInfos):
                    self?.pokemon = pokemonInfos.compactMap { $0.url }.map { PokemonViewModel(pokemonUrl: $0) }
                case .failure(let error):
                    self?.error = error
                }
            }
        }
    }
}
