//
//  PokedexView.swift
//  Pokedex
//
//  Created by Mazen Denden on 13/7/2022.
//

import SwiftUI

struct PokedexView: View {
    
    @StateObject private var pokedexViewModel = PokedexViewModel()
    
    var body: some View {
        VStack {
            if pokedexViewModel.error != nil {
                Text(pokedexViewModel.error?.description ?? "")
                    .padding(4)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity)
                    .background(Color.red)
                    .clipShape(Rectangle())
            }
            
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: pokedexViewModel.columns, spacing: 12) {
                    ForEach(pokedexViewModel.pokemon) { viewModel in
                        NavigationLink {
                            PokemonDetailsView(pokemonViewModel: viewModel)
                        } label: {
                            PokemonCell(viewModel: viewModel)
                        }
                    }
                }
                .padding()
            }
        }
        .navigationBarTitle("Pokedex", displayMode: .large)
        .onAppear {
            pokedexViewModel.fetchPokemon(limit: 150, offset: 0)
        }
    }
    
}

struct Pokedex_Previews: PreviewProvider {
    static var previews: some View {
        PokedexView()
    }
}
