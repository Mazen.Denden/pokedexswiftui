//
//  DetailsTabBarItem.swift
//  Pokedex
//
//  Created by Mazen Denden on 18/7/2022.
//

import SwiftUI

struct DetailsTabBarItem: View {
    
    @Binding var currentTab: DetailsViewTab
    let namespace: Namespace.ID
    
    var tabBarItemName: String
    var tab: DetailsViewTab
    let pokemonColor: Color
    
    var body: some View {
        Button {
            self.currentTab = tab
        } label: {
            VStack {
                Spacer()
                
                Text(tabBarItemName)
                    .font(.system(size: 16,
                                  weight: currentTab == tab ? .medium : .regular))
                
                if currentTab == tab {
                    pokemonColor
                        .frame(height: 2)
                        .matchedGeometryEffect(id: "underline",
                                               in: namespace,
                                               properties: .frame)
                } else {
                    Color.clear
                        .frame(height: 2)
                }
            }
            .animation(.spring(), value: self.currentTab)
        }
        .buttonStyle(.plain)
    }
}
