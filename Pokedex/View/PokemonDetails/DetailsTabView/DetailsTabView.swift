//
//  DetailsTabView.swift
//  Pokedex
//
//  Created by Mazen Denden on 18/7/2022.
//

import SwiftUI

enum DetailsViewTab: Int, CaseIterable {
    case about = 0
    case baseStats
    case evolutions
    case moves
}

extension DetailsViewTab: CustomStringConvertible {
    var description: String {
        switch self {
        case .about: return "About"
        case .baseStats: return "Base Stats"
        case .evolutions: return "Evolutions"
        case .moves: return "Moves"
        }
    }
}

struct DetailsTabView: View {
    let tabs: [DetailsViewTab] = DetailsViewTab.allCases
    let pokemonColor: Color
    
    @Binding var currentTab: DetailsViewTab
    @Namespace var namespace
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 24) {
                ForEach(tabs, id: \.rawValue) { tab in
                    DetailsTabBarItem(currentTab: self.$currentTab,
                                      namespace: namespace.self,
                                      tabBarItemName: tab.description,
                                      tab: tab,
                                      pokemonColor: pokemonColor)
                }
            }
            .padding(.horizontal)
        }
    }
    
}
