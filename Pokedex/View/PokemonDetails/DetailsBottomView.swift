//
//  DetailsBottomView.swift
//  Pokedex
//
//  Created by Mazen Denden on 18/7/2022.
//

import SwiftUI

struct DetailsBottomView: View {
    
    @State var currentTab: DetailsViewTab = .about
    let pokemonColor: Color
    
    let pokemonViewModel: PokemonViewModel
    
    var body: some View {
        ZStack(alignment: .top) {
            TabView(selection: $currentTab) {
                AboutView(pokemonViewModel: pokemonViewModel).tag(DetailsViewTab.about)
                BaseStatsView(pokemonViewModel: pokemonViewModel).tag(DetailsViewTab.baseStats)
                Text("Work in progress").tag(DetailsViewTab.evolutions)
                Text("Work in progress").tag(DetailsViewTab.moves)
            }
            .tabViewStyle(.page(indexDisplayMode: .never))
            
            DetailsTabView(pokemonColor: pokemonColor, currentTab: $currentTab)
                .frame(height: 80)
        }
    }
}

struct PokemonDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonDetailsView(pokemonViewModel:
                            PokemonViewModel(pokemon: MockData.singlePokemon())
        )
    }
}

struct AboutView: View {
    
    @StateObject var pokemonViewModel: PokemonViewModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 14) {
            Text(pokemonViewModel.flavorText)
                .font(.system(size: 14, weight: .medium))
            
            AboutRow(title: "Abilities", value: pokemonViewModel.abilities)
            
            SizeView(pokemonViewModel: pokemonViewModel)
            
            Section("Breeding") {
                AboutRow(title: "Egg Groups", value: pokemonViewModel.eggGroups)
                AboutRow(title: "Habitat", value: pokemonViewModel.habitat)
            }
        }
        .padding()
        .onAppear {
            pokemonViewModel.fetchPokemonSpecie()
        }
    }
}

struct AboutRow: View {
    
    let title: String
    let value: String
    
    var body: some View {
        HStack(spacing: 0) {
            Text(title)
                .font(.system(size: 16, weight: .regular))
                .foregroundColor(.gray)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Text(value)
                .font(.system(size: 16, weight: .regular))
                .frame(maxWidth: .infinity, alignment: .leading)
        }
    }
}

struct BaseStatsView: View {
    
    let pokemonViewModel: PokemonViewModel
    
    var body: some View {
        VStack {
            ForEach(pokemonViewModel.stats, id: \.stat?.name) { pokemonStat in
                let progress: Float = Float(Float(pokemonStat.baseStat) / 255)
                StatProgressView(pokemonStat: pokemonStat, progress: progress)
            }
        }
        .padding()
    }
}

struct StatProgressView: View {
    
    let pokemonStat: PokemonStat
    let progress: Float
    
    var body: some View {
        HStack(spacing: 16) {
            Text(pokemonStat.stat?.name.capitalized ?? "")
                .font(.system(size: 16, weight: .regular))
                .foregroundColor(.gray)
                .frame(maxWidth: 120, alignment: .leading)
            
            Text("\(pokemonStat.baseStat)")
                .font(.system(size: 16, weight: .regular))
                .frame(maxWidth: 30, alignment: .leading)
            
            ProgressView(value: progress)
                .tint(progress < 0.5 ? .red : .green)
        }
    }
}

struct SizeView: View {
    
    let pokemonViewModel: PokemonViewModel
    
    var body: some View {
        VStack(spacing: 4) {
            HStack {
                Text("Height")
                    .font(.system(size: 16, weight: .regular))
                    .foregroundColor(.gray)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text("Weight")
                    .font(.system(size: 16, weight: .regular))
                    .foregroundColor(.gray)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            
            HStack {
                Text(pokemonViewModel.height)
                    .font(.system(size: 16, weight: .regular))
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text(pokemonViewModel.weight)
                    .font(.system(size: 16, weight: .regular))
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
        }
        .padding()
        .background(Color.white)
        .cornerRadius(15)
        .shadow(color: .black.opacity(0.1), radius: 15, x: 0, y: 0)
    }
}
