//
//  PokemonDetailsView.swift
//  Pokedex
//
//  Created by Mazen Denden on 15/7/2022.
//

import SwiftUI
import Kingfisher

struct PokemonDetailsView: View {
    
    let pokemonViewModel: PokemonViewModel
    
    var body: some View {
        ZStack {
            pokemonViewModel.pokemonTypeColor
                .ignoresSafeArea(.all, edges: .top)
            
            GeometryReader { proxy in
                VStack {
                    TitleView(pokemonViewModel: pokemonViewModel)
                        .padding(.horizontal, 24)
                    
                    Spacer()
                    
                    ZStack {
                        DetailsView(pokemonViewModel: pokemonViewModel)
                            .cornerRadius(30, corners: [.topLeft, .topRight])
                        
                        VStack {
                            if let url = pokemonViewModel.pokemonImageURL {
                                KingFisherImage(url: url)
                                    .frame(width: 240)
                                    .offset(y: -180)
                            }
                            
                            Spacer()
                        }
                    }
                    .frame(maxHeight: proxy.size.height * 0.65)
                }
                .navigationBarTitle("", displayMode: .inline)
                .ignoresSafeArea(.all, edges: .bottom)
            }
        }
        
    }
    
    struct PokemonDetailsView_Previews: PreviewProvider {
        static var previews: some View {
            PokemonDetailsView(pokemonViewModel:
                                PokemonViewModel(pokemon: MockData.singlePokemon())
            )
        }
    }
}

struct KingFisherImage: View {
    let url: URL
    var aspect = ContentMode.fit
    
    var body: some View {
        KFImage.url(url)
            .placeholder {
                Image("pokeball")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 240)
            }
            .loadDiskFileSynchronously()
            .cacheMemoryOnly()
            .fade(duration: 0.25)
            .resizable()
            .aspectRatio(contentMode: aspect)
        
    }
}

struct TitleView: View {
    
    let pokemonViewModel: PokemonViewModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 8) {
                Text(pokemonViewModel.name)
                    .font(.title)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                
                HStack {
                    ForEach(pokemonViewModel.types, id: \.self) { type in
                        Text(type)
                            .font(.system(size: 12, weight: .regular))
                            .foregroundColor(.white)
                            .padding(.horizontal, 12)
                            .frame(height: 30)
                            .background {
                                RoundedRectangle(cornerRadius: 15, style: .continuous)
                                    .fill(.white.opacity(0.2))
                            }
                    }
                }
            }
            
            Spacer()
            
            HStack {
                Spacer()
                
                Text(pokemonViewModel.numberString)
                    .font(.system(size: 16, weight: .semibold))
                    .foregroundColor(.white.opacity(0.5))
            }
            .padding()
        }
    }
}

struct DetailsView: View {
    
    let pokemonViewModel: PokemonViewModel
    
    var body: some View {
        VStack {
            DetailsBottomView(pokemonColor: pokemonViewModel.pokemonTypeColor,
                              pokemonViewModel: pokemonViewModel)
        }
        .padding()
        .background {
            Color.white
        }
    }
}
