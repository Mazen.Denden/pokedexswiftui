//
//  PokedexListService.swift
//  Pokedex
//
//  Created by Mazen Denden on 17/8/2022.
//

import Foundation

class PokedexListService: PokedexListServiceProtocol {
    let networkManager: NetworkManagerProtocol
    init(networkManager: NetworkManagerProtocol = NetworkManager.shared) {
        self.networkManager = networkManager
    }
}

extension PokedexListService {
    func fetchPokemon(limit: Int, offset: Int, completion: @escaping (Result<[PokemonInfo], ApiError>) -> Void) {
        let endPoint = Endpoint.pokemonList(limit: limit, offset: offset)
        networkManager.fetch(type: PaginationModel<PokemonInfo>.self, from: endPoint) { result in
            switch result {
            case .success(let paginationModel):
                completion(.success(paginationModel.results))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
