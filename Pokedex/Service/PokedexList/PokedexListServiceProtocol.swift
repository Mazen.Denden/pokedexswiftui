//
//  PokedexListServiceProtocol.swift
//  Pokedex
//
//  Created by Mazen Denden on 17/8/2022.
//

import Foundation

protocol PokedexListServiceProtocol: AnyObject {
    func fetchPokemon(limit: Int, offset: Int, completion: @escaping (Result<[PokemonInfo], ApiError>) -> Void)
}
