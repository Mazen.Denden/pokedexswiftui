//
//  EndPoint+PokedexList.swift
//  Pokedex
//
//  Created by Mazen Denden on 17/8/2022.
//

import Foundation

extension Endpoint {
    static func pokemonList(limit: Int, offset: Int) -> Endpoint {
        let queryItems = [
            URLQueryItem(name: "offset", value: "\(offset)"),
            URLQueryItem(name: "limit", value: "\(limit)")
        ]
        return Endpoint(path: "/api/v2/pokemon", queryItems: queryItems)
    }
}
