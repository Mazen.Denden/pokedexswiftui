//
//  Pokemon.swift
//  Pokedex
//
//  Created by Mazen Denden on 14/7/2022.
//

import SwiftUI

struct PokemonInfo: Codable {
    var name: String = ""
    var url: String? = ""
}

struct Pokemon: Codable {
    var id: Int = 0
    var abilities: [PokemonAbility] = []
    var name: String = ""
    var species: Specie?
    var sprites: PokemonSprite?
    var stats: [PokemonStat] = []
    var types: [PokemonTypeSlot] = []
    var height: Int = 0
    var weight: Int = 0
}

struct PokemonStat: Codable {
    var baseStat: Int = 0
    var stat: Stat?
    
    enum CodingKeys: String, CodingKey {
        case stat
        case baseStat = "base_stat"
    }
}

struct Stat: Codable {
    var name: String = ""
}

struct Specie: Codable {
    var url: String? = ""
    var genera: [Genus]? = []
    var eggGroups: [EggGroup]? = []
    var flavorTextEntries: [FlavorText]? = []
    var habitat: Habitat?
    
    enum CodingKeys: String, CodingKey {
        case url, genera, habitat
        case eggGroups = "egg_groups"
        case flavorTextEntries = "flavor_text_entries"
    }
}

struct FlavorText: Codable {
    var flavorText: String = ""
    var language: Language?
    
    enum CodingKeys: String, CodingKey {
        case language
        case flavorText = "flavor_text"
    }
}

struct Genus: Codable {
    var genus: String = ""
    var language: Language?
}

struct Language: Codable {
    var name: String = ""
}

struct EggGroup: Codable {
    var name: String = ""
}

struct Habitat: Codable {
    var name: String = ""
}

struct PokemonAbility: Codable {
    var ability: Ability?
}

struct Ability: Codable {
    var name: String = ""
}

struct PokemonTypeSlot: Codable {
    var slot: Int = 0
    var type: PokemonType?
}

struct PokemonType: Codable {
    var name: String = ""
    var url: String = ""
    
    var color: Color {
        return TypeColor(rawValue: name)?.color ?? Color.gray.opacity(0.2)
    }
}

struct PokemonSprite: Codable {
    var other: PokemonArtwork?
}

struct PokemonArtwork: Codable {
    var officialArtwork: OfficialArtwork?
    
    enum CodingKeys: String, CodingKey {
        case officialArtwork = "official-artwork"
    }
}

struct OfficialArtwork: Codable {
    var frontDefault: String = ""
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
}

struct PaginationModel<T: Codable>: Codable {
    var count: Int? = 0
    var next: String? = ""
    var previous: String? = ""
    var results: [T] = []
}
