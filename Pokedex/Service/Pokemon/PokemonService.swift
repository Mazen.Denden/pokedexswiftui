//
//  PokemonService.swift
//  Pokedex
//
//  Created by Mazen Denden on 17/8/2022.
//

import Foundation

final class PokemonService: PokemonServiceProtocol {
    let networkManager: NetworkManagerProtocol
    init(networkManager: NetworkManagerProtocol = NetworkManager.shared) {
        self.networkManager = networkManager
    }
}

extension PokemonService {
    func fetchPokemon(pokemonUrl: String, completion: @escaping (Result<Pokemon, ApiError>) -> Void) {
        networkManager.fetch(type: Pokemon.self, from: URL(string: pokemonUrl)) { result in
            switch result {
            case .success(let pokemon):
                completion(.success(pokemon))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchPokemonSpecie(specieURL: String, completion: @escaping (Result<Specie, ApiError>) -> Void) {
        networkManager.fetch(type: Specie.self, from: URL(string: specieURL)) { result in
            switch result {
            case .success(let specie):
                completion(.success(specie))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
//    func fetchPokemon(limit: Int, offset: Int, completion: @escaping (Result<[PokemonInfo], ApiError>) -> Void) {
//        let endPoint = Endpoint.pokemonList(limit: limit, offset: offset)
//        networkManager.fetch(type: PaginationModel<PokemonInfo>.self, from: endPoint) { result in
//            switch result {
//            case .success(let paginationModel):
//                completion(.success(paginationModel.results))
//            case .failure(let error):
//                completion(.failure(error))
//            }
//        }
//    }
}
