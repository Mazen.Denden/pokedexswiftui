//
//  PokemonServiceProtocol.swift
//  Pokedex
//
//  Created by Mazen Denden on 17/8/2022.
//

import Foundation

protocol PokemonServiceProtocol: AnyObject {
    func fetchPokemon(pokemonUrl: String, completion: @escaping (Result<Pokemon, ApiError>) -> Void)
    func fetchPokemonSpecie(specieURL: String, completion: @escaping (Result<Specie, ApiError>) -> Void)
}
