//
//  PokedexApp.swift
//  Pokedex
//
//  Created by Mazen Denden on 2/6/2022.
//

import SwiftUI

@main
struct PokedexApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
